# PgJson library
An library to communicate with PostgreSQL server for JSON data storage. 

## Why this library?
This library and client allows user to store data in PostgreSQL server in form of JSON.
To index particular JSON attributes and make fast searches over data. CRUD operation are doable with stored data.
In other words this library implements document oriented data storage on PostgreSQL where one JSON object stored in 
one row is considered as one data entry.
By default it is suggested to store same type objects in deferent tables. But library support storing different objects with different 
schemas in same table. For last feature side effect is that all attributes in same table that is indexed must be witih same name and same index type.

## Building

```shell
./gradlew clean jar
```
## Running test
For running test Docker must be installed in environment, Testecontainers are used to run PostgreSQl server background.

```shell
./gradlew clean test
```

## How to use this library?

Code epository has lot of test, see test to understand how searching etc works.

Code snippet for initializing PgJson client in Spring Boot application:
```java
@Configuration
@Slf4j
public class PgJsonClientConfig {

    private static String poolName = "pgjson-pool";
    private PgJsonClient pgJsonClient;

    @Bean
    public PgJsonClient createPgJsonClient() {
        Properties props = new Properties();
        props.setProperty("dataSourceClassName", "{VALUE}");
        props.setProperty("maximumPoolSize", "{VALUE}");
        props.setProperty("dataSource.user", "{VALUE}");
        props.setProperty("dataSource.password", "{VALUE}");
        props.setProperty("dataSource.databaseName", "{VALUE}");
        props.setProperty("dataSource.portNumber", "{VALUE}");
        props.setProperty("dataSource.serverName", "{VALUE}");
        props.setProperty("maxLifetime", "{VALUE}");
        props.setProperty("poolName", poolName);
        props.setProperty("minimumIdle", "{VALUE}");
        props.setProperty("idleTimeout", "{VALUE}");
        props.setProperty("connectionTimeout", "{VALUE}");

        pgJsonClient = new PgJsonClient(props);
        return pgJsonClient;
    }

    public PgJsonClient getPgJsonClient() {
        try {
            return this.pgJsonClient;
        } catch (Exception exc) {
            return null;
        }
    }

    @PreDestroy
    private void closeClass() {
        try {
            log.info("Cleaning DatabaseService object and closing database connections.");
            if (pgJsonClient != null) {
                pgJsonClient.close();
            }
        } catch (Exception exc) {
            log.error("Method closeClass failed with error", exc);
        }
    }
}
``` 
in order to use this client in Service class:
```java
@Autowired PgJsonClientConfig pgJsonClientHolder;

List<DatabaseEntry> result = pgJsonClientHolder
   .getPgJsonClient()
   .selectData("{database table name}", "{search term}");
``` 

For all CRUD operatsions there is several unit test that can be followed to dig deeper.

## Indexing databse

### index name

Index name is important to make library work as intended. Iadex name format: pgjson_{index type}_{table name}_{attribute key}

Index types:
* exact - btree index to exactly match attribute value
* object - indexed object
* fts - full text search index where json value will be indexed for Full Text Search
* nestedobject - to index objects nested object attribute key

Atr this time library is not able to create indexes automatically based schema definition. This feature is planned.

Creating indexes manually:
```
CREATE INDEX pgjson_exact_table1_attribute1 ON public.table1 ((json_data ->>'attribute1'));

CREATE INDEX pgjson_object_table1_attribute3 ON public.table1 USING gin((json_data->'attribute3') jsonb_path_ops);

CREATE INDEX pgjson_fts_table1_attribute2 ON public.table1 USING gin(to_tsvector('simple', json_data->'attribute2'));

CREATE INDEX pgjson_nestedobject_table3_attribute1_attribute11_attribute111 ON public.table3 USING gin((json_data->'attribute1'->'attribute11'->'attribute111') jsonb_path_ops);
```

## Accessing data by search
Consider last sample we have two data entryes:
```json
{
  "attribute1": "AAAAA",
  "attribute2": "BBBBB"
}
```
and
```json
{
  "attribute1": "CCCCC",
  "attribute2": "DDDDD"
}
```
attribute1 has PostgreSQL betree index on json_data->>'attribute1', table name is table1 and
attribute2 has PostgreSQL betree index on json_data->>'attribute2',
in this case search term will be :
```json
{
  "limit": 1,
  "offset": 0,
  "searchTerm": {
    "attribute1" : "AAAAA",
    "attribute2" : "DDDDD",
    "logicalOperator": "OR"
  }
}
```

Attribute logicalOperator by default is AND (if attribute is not present at all).
Before executing this search, an collection of two DatabaseEntry object will be returned.

# License
MIT License

Copyright (c) 2023 Avalanche Laboratory

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.