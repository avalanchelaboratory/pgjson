package io.bitbucket.avalanchelaboratory.pgjson.util;

import io.bitbucket.avalanchelaboratory.pgjson.PostgreSqlJsonClient;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Properties;

@Slf4j
@Testcontainers
public abstract class DatabaseConfigurationUtil {

    public static PostgreSqlJsonClient postgresqlJsonClient;
    private static Properties initProps;

    FileUtil fileUtil = new FileUtil();

    @Container
    public static PostgreSQLContainer postgres =
            new PostgreSQLContainer<>(TestContainerConfiguration.POSTGRESQL_VERSION)
                    .withDatabaseName(TestContainerConfiguration.POSTGRESQL_DATABASE_NAME)
                    .withUsername(TestContainerConfiguration.POSTGRESQL_USER_NAME)
                    .withPassword(TestContainerConfiguration.POSTGRESQL_PASSWORD)
                    .withInitScript("sql/init.sql")
                    .withCommand("postgres -c max_connections=150");

    public DatabaseConfigurationUtil() {
        initProps = new Properties();
        initProps.setProperty("dataSource.user", postgres.getUsername());
        initProps.setProperty("dataSource.password", postgres.getPassword());
        initProps.setProperty("jdbcUrl", postgres.getJdbcUrl());
    }

    @BeforeEach
    public void connectClient() {
        Properties props = new Properties();
        props.setProperty("dataSource.user", postgres.getUsername());
        props.setProperty("dataSource.password", postgres.getPassword());
        props.setProperty("jdbcUrl", postgres.getJdbcUrl());
        log.info("Connecting to PostgreSqlJsonClient with parameters: {}", props);
        postgresqlJsonClient = new PostgreSqlJsonClient(props);
        fileUtil = new FileUtil();
    }
}
