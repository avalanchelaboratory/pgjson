package io.bitbucket.avalanchelaboratory.pgjson.util;

import io.bitbucket.avalanchelaboratory.pgjson.model.validation.ValidationResult;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Slf4j
public class ValidationUtilTest {

  private ValidationUtil validationUtil = new ValidationUtil();

  private FileUtil fileUtil = new FileUtil();

  @Test
  public void validateDataPositiveScenario() throws IOException {

    String data = fileUtil.readStringFromFile("json/sample_data.json");
    String schema = fileUtil.readStringFromFile("json/test-schema.json");
    ValidationResult result = validationUtil.validateData(schema, data);
    log.info("Result: {}", result);
    assertTrue(result.getValidationStatus());
  }

  @Test
  public void validateDataNegativeScenario() throws IOException {

    String data = fileUtil.readStringFromFile("json/sample_data_not_valid.json");
    String schema = fileUtil.readStringFromFile("json/test-schema.json");
    ValidationResult result = validationUtil.validateData(schema, data);
    log.info("Result: {}", result);
    assertFalse(result.getValidationStatus());
  }

  @Test
  public void validateDataPositiveScenario1() throws IOException {

    String data = fileUtil.readStringFromFile("json/test_schema2_data.json");
    String schema = fileUtil.readStringFromFile("json/test_schema2.json");
    ValidationResult result = validationUtil.validateData(schema, data);
    log.info("Result: {}", result);
    assertTrue(result.getValidationStatus());
  }

}
