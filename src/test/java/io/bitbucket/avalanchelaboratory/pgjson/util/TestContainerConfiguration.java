package io.bitbucket.avalanchelaboratory.pgjson.util;

public class TestContainerConfiguration {
    public static final String POSTGRESQL_VERSION = "postgres:13.3";
    public static final String POSTGRESQL_DATABASE_NAME = "pgjson";
    public static final String POSTGRESQL_USER_NAME = "pgjsonuser";
    public static final String POSTGRESQL_PASSWORD = "pgjsonuser";
}
