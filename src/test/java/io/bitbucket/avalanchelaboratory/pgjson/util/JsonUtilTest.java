package io.bitbucket.avalanchelaboratory.pgjson.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class JsonUtilTest {

  JsonUtil jsonUtil = new JsonUtil();
  Gson gson = new Gson();

  @Test
  public void restrict_mergingTest() {
    String objectOld = "{\"A\":\"A\"}";
    String objectNew = "{\"A\":\"X\", \"B\":\"X\"}";

    List<String> restrictions = new ArrayList<>();
    restrictions.add("A");

    String mergedObject = jsonUtil.mergeObjects(restrictions, objectOld, objectNew);
    log.debug(mergedObject);
  }

  @Test
  public void concat_two_objects_mergeObjectsTest() {

    String object1 = "{\"A\":\"A\"}";
    String object2 = "{\"B\":\"B\"}";

    String mergedObject = jsonUtil.mergeObjects(object1, object2);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    assertEquals("A", jsonObject.get("A").getAsString());
    assertEquals("B", jsonObject.get("B").getAsString());
  }

  @Test
  public void rewrite_mergeObjectsTest() {

    String objectOld = "{\"A\":\"A\"}";
    String objectNew = "{\"A\":\"X\"}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    assertEquals("X", jsonObject.get("A").getAsString());
  }

  @Test
  public void null_mergeObjectsTest() {

    String objectOld = "{\"A\":\"A\"}";
    String objectNew = "{\"A\":null}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    assertTrue(jsonObject.entrySet().size() == 0);
  }

  @Test
  public void null2_mergeObjectsTest() {

    // {"A": "A", "B": {"C": "C", "D": "D"}}
    // {"A": "A", "B": null}
    String objectOld = "{\"A\": \"A\", \"B\": {\"C\": \"C\", \"D\": \"D\"}}";
    String objectNew = "{\"A\": \"A\", \"B\": null}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    assertEquals("A", jsonObject.get("A").getAsString());
    assertTrue(jsonObject.entrySet().size() == 1);
  }

  @Test
  public void not_allow_null_mergeObjectsTest() {

    String objectOld = "{\"A\":\"A\"}";
    String objectNew = "{\"B\":null}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    assertEquals("A", jsonObject.get("A").getAsString());
    assertTrue(jsonObject.entrySet().size() == 1);
  }

  @Test
  public void cahnge_in_object_mergeObjectsTest() {

    // {"A": "A", "B": {"C": "C", "D": "D"}}
    // {"A": "A", "B": {"C": "C", "D": "X"}}
    String objectOld = "{\"A\": \"A\", \"B\": {\"C\": \"C\", \"D\": \"D\"}}";
    String objectNew = "{\"A\": \"A\", \"B\": {\"C\": \"C\", \"D\": \"X\"}}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    assertEquals("A", jsonObject.get("A").getAsString());
    JsonObject B = jsonObject.get("B").getAsJsonObject();
    assertEquals("C", B.get("C").getAsString());
    assertEquals("X", B.get("D").getAsString());
  }

  @Test
  public void concat_arrays_mergeObjectsTest() {
    String objectOld = "[{\"A\": \"A\"}]";
    String objectNew = "[{\"B\": \"B\"}]";

    String mergedObject = jsonUtil.mergeArrays(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonArray array = gson.fromJson(mergedObject, JsonArray.class);
    assertTrue(array.size() == 2);
  }

  @Test
  public void concat_object_with_arrays_mergeObjectsTest() {
    // {"Array":[{"A": "A"}]}
    // {"Array":[{"B": "B"}]}
    String objectOld = "{\"Array\":[{\"A\": \"A\"}]}";
    String objectNew = "{\"Array\":[{\"B\": \"B\"}]}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    JsonArray array = jsonObject.get("Array").getAsJsonArray();
    assertTrue(array.size() == 2);
  }

  @Test
  public void delete_array_element_mergeObjectsTest() {
    String objectOld = "{\"Array\":[{\"A\": \"A\"}, {\"B\": \"B\"}]}";
    String objectNew = "{\"Array\":[null, {\"B\": \"B\"}]}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    JsonArray array = jsonObject.get("Array").getAsJsonArray();
    assertTrue(array.size() == 1);
  }

  @Test
  public void delete_array_element2_mergeObjectsTest() {
    String objectOld = "{\"Array\":[{\"A\": \"A\"}, {\"B\": \"B\"}, {\"C\": \"C\"}]}";
    String objectNew = "{\"Array\":[{\"A\": \"A\"}, null, {\"C\": \"C\"}]}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    JsonArray array = jsonObject.get("Array").getAsJsonArray();
    assertTrue(array.size() == 2);
  }

  @Test
  public void delete_one_array_and_add_new_mergeObjectsTest() {
    String objectOld = "{\"Array\":[{\"A\": \"A\"}, {\"B\": \"B\"}]}";
    String objectNew = "{\"Array\":[{\"A\": \"A\"}, null, {\"C\": \"C\"}]}";

    String mergedObject = jsonUtil.mergeObjects(objectOld, objectNew);
    assertNotNull(mergedObject);
    log.debug("Merged object: {}", mergedObject);

    JsonObject jsonObject = gson.fromJson(mergedObject, JsonObject.class);
    JsonArray array = jsonObject.get("Array").getAsJsonArray();
    assertTrue(array.size() == 2);
  }
}
