package io.bitbucket.avalanchelaboratory.pgjson.util;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
public class IndexUtilTest {

  IndexUtil indexUtil = new IndexUtil();
  FileUtil fileUtil = new FileUtil();

  @Test
  public void getIndexes() throws Exception {
    String schema = fileUtil.readStringFromFile("json/test-schema.json");
    indexUtil.getIndexes(schema);
  }
}
