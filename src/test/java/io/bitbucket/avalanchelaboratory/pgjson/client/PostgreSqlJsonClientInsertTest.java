package io.bitbucket.avalanchelaboratory.pgjson.client;

import io.bitbucket.avalanchelaboratory.pgjson.model.operations.Result;
import io.bitbucket.avalanchelaboratory.pgjson.util.DatabaseConfigurationUtil;
import io.bitbucket.avalanchelaboratory.pgjson.util.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class PostgreSqlJsonClientInsertTest extends DatabaseConfigurationUtil {

    FileUtil fileUtil = new FileUtil();

    String tableName = "table1";

    @Test
    public void insertData() throws java.io.IOException {
        String fileData = fileUtil.readStringFromFile("json/sample_data.json");
        Result result = postgresqlJsonClient.insertData(tableName, fileData);
        log.info("Result: {}", result);
        assertNotNull(result);
        assertEquals(true, result.getResultStatus());
    }

    @Test
    public void insertDataFailsValidation() throws java.io.IOException {
        String fileData = fileUtil.readStringFromFile("json/sample_data_not_valid.json");
        Result result = postgresqlJsonClient.insertData(tableName, fileData);
        log.info("Result: {}", result);
        assertNotNull(result);
        assertEquals(false, result.getResultStatus());
        assertNull(result.getIduuid());
    }

    // table2_schema1
    @Test
    public void insertDataToTableWithMultipleSchema() throws java.io.IOException {
        String fileData = fileUtil.readStringFromFile("json/insert_table2_schema1.json");
        Result result = postgresqlJsonClient.insertData("table2", "table2_schema1", fileData);
        log.info("Result: {}", result);
        assertNotNull(result);
        assertEquals(true, result.getResultStatus());

        String fileData2 = fileUtil.readStringFromFile("json/insert_table2_schema2_v2.json");
        Result result2 = postgresqlJsonClient.insertData("table2", "table2_schema2", fileData2);
        log.info("Result: {}", result2);
        assertNotNull(result2);
        assertEquals(true, result2.getResultStatus());

        String fileData3 = fileUtil.readStringFromFile("json/insert_table2_schema2_v1.json");
        Result result3 = postgresqlJsonClient.insertData("table2", "table2_schema2", fileData3);
        log.info("Result: {}", result3);
        assertNotNull(result3);
        assertEquals(false, result3.getResultStatus());
    }
}
