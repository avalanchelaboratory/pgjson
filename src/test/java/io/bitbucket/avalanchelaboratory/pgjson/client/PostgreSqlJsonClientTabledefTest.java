package io.bitbucket.avalanchelaboratory.pgjson.client;

import io.bitbucket.avalanchelaboratory.pgjson.model.TableDef;
import io.bitbucket.avalanchelaboratory.pgjson.util.DatabaseConfigurationUtil;
import io.bitbucket.avalanchelaboratory.pgjson.util.FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class PostgreSqlJsonClientTabledefTest extends DatabaseConfigurationUtil {

    FileUtil fileUtil = new FileUtil();

    @Test
    public void getTableDefById() {
        Integer tableDefId = 1;
        TableDef tableDef = postgresqlJsonClient.getTableDefByIdFromMemory(tableDefId);
        log.debug("Tabledef: {}", tableDef);
        assertNotNull(tableDef);
        assertEquals(tableDefId, tableDef.getTableDefId());
    }

    @Test
    public void getTableDefByIdUuid() {
        String idUuid = "6dc4bf18-49e7-4c10-8a23-a4752e48f6d0";
        TableDef tableDef = postgresqlJsonClient.getTableDefByIdUuid(idUuid);
        assertNotNull(tableDef);
        assertEquals(idUuid, tableDef.getIdUuid());
    }

    @Test
    public void getTableDefByIdUuidFromMemory() {
        String idUuid = "6dc4bf18-49e7-4c10-8a23-a4752e48f6d0";
        String tableName = "table1";
        TableDef tableDef = postgresqlJsonClient.getTableDefByIdUuidFromMemory(idUuid);
        assertNotNull(tableDef);
        assertEquals(idUuid, tableDef.getIdUuid());
        assertEquals(tableName, tableDef.getTableName());
    }

    @Test
    public void insertSchema() throws java.io.IOException {

        String tableName = "table1";
        String schemaName = "table1_schema-insert";
        String schemaData = fileUtil.readStringFromFile("json/test-schema-insert.json");

        String result = postgresqlJsonClient.insertSchema(tableName, schemaName, schemaData);
        log.info("Result: {}", result);
        assertNotNull(result);
    }

    @Test
    public void isDatabaseRunning() {
        boolean result = postgresqlJsonClient.isDatabaseRunning();
        assertTrue(result);
    }
}
