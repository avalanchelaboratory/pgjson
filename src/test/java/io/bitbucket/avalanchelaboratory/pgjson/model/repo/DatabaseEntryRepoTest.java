package io.bitbucket.avalanchelaboratory.pgjson.model.repo;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import io.bitbucket.avalanchelaboratory.pgjson.exceptions.PostgreJsonException;
import java.util.ArrayList;

import io.bitbucket.avalanchelaboratory.pgjson.model.enums.LogicalOperator;
import org.junit.jupiter.api.Test;

public class DatabaseEntryRepoTest {

  DatabaseEntryRepo repo = new DatabaseEntryRepo();

  @Test
  public void createTermTest() {
    JsonObject object = new JsonObject();
    JsonElement element = new JsonPrimitive("John");
    object.add("attribute1", element);

    PostgreJsonException thrown = assertThrows(
        PostgreJsonException.class,
        () -> repo.createTerm(object, new ArrayList<>(), 1, LogicalOperator.and),
        "Expected PostgreJsonException"
    );

    assertTrue(thrown.getMessage().equals("Could not generate search term"));
  }
}
