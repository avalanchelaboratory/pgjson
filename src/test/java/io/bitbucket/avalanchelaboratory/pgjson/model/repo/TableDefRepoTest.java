package io.bitbucket.avalanchelaboratory.pgjson.model.repo;

import io.bitbucket.avalanchelaboratory.pgjson.model.TableDef;
import io.bitbucket.avalanchelaboratory.pgjson.util.DatabaseConfigurationUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
public class TableDefRepoTest extends DatabaseConfigurationUtil {

    TableDefRepo repo = new TableDefRepo();

    @Test
    public void selectAllEffectiveTableDefTest() {
        ArrayList<TableDef> result = repo.selectAllEffectiveTableDef(postgresqlJsonClient.getConnection());
        log.info("Table defs: {}", result);
        assertEquals(4, result.size());
        assertEquals("6dc4bf18-49e7-4c10-8a23-a4752e48f6d0", result.get(0).getIdUuid());
        assertEquals("6a4ab7da-eb74-4280-a0a2-990feff6f836", result.get(1).getIdUuid());
        assertEquals("e51e38d9-4a1e-427b-88fc-4026dfd576ec", result.get(2).getIdUuid());
    }

    @Test
    public void selectTableDefByTableName_only_one_schema_per_Table_Test() {
        TableDef tableDef = repo.selectTableDefByTableName(postgresqlJsonClient.getConnection(), "table1");
        assertNotNull(tableDef);
    }

    @Test
    public void selectTableDefByTableName_multiple_schema_per_table_Test() {
        TableDef tableDef = repo.selectTableDefByTableName(postgresqlJsonClient.getConnection(), "table2");
        assertNotNull(tableDef);
    }
}
