package io.bitbucket.avalanchelaboratory.pgjson.exceptions;

public class RequestException extends Exception {

    public RequestException(String exceptionJson) {
        super(exceptionJson);
    }
}
