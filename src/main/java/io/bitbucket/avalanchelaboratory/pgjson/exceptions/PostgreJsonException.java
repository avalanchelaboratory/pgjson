package io.bitbucket.avalanchelaboratory.pgjson.exceptions;

public class PostgreJsonException extends Exception {

    public PostgreJsonException(String message) {
        super(message);
    }
}
