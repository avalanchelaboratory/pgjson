package io.bitbucket.avalanchelaboratory.pgjson.model.repo;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.bitbucket.avalanchelaboratory.pgjson.exceptions.PostgreJsonException;
import io.bitbucket.avalanchelaboratory.pgjson.model.DatabaseEntry;
import io.bitbucket.avalanchelaboratory.pgjson.model.IndexInfo;
import io.bitbucket.avalanchelaboratory.pgjson.model.enums.IndexType;
import io.bitbucket.avalanchelaboratory.pgjson.model.enums.LogicalOperator;
import io.bitbucket.avalanchelaboratory.pgjson.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class DatabaseEntryRepo {

    protected static final String INSERT_DATA;
    protected static final String SELECT_ALL;
    protected static final String SELECT_ALL_BY_TERM;
    protected static final String SELECT_ALL_BY_TERM_WITH_LIMIT;
    protected static final String SELECT_BY_IDUUID;
    protected static final String UPDATE_BY_UUID;
    protected static final String DELETE_ARRAY_ELEMENT;
    protected static final String DELETE_RECORD;

    private DateUtil dateUtil = new DateUtil();

    static {
        INSERT_DATA =
                "INSERT INTO public.%s (tabledef_id, json_data, id_uuid, data_created, data_changed) VALUES (?, ?::JSON, ?, ?, ?) RETURNING *;";

        SELECT_ALL = "SELECT * FROM public.%s ORDER BY id %s LIMIT ? OFFSET ?;";

        SELECT_ALL_BY_TERM = "SELECT * FROM public.%s WHERE %s;";

        SELECT_ALL_BY_TERM_WITH_LIMIT = "SELECT * FROM public.%s WHERE %s ORDER BY id %s LIMIT ? OFFSET ?;";

        SELECT_BY_IDUUID = "SELECT * FROM public.%s WHERE id_uuid=?;";

        UPDATE_BY_UUID = "UPDATE public.%s SET json_data=?::JSON, data_changed=NOW()::timestamp WHERE id_uuid=? RETURNING *;";

        DELETE_ARRAY_ELEMENT =
                "UPDATE public.%s"
                        + " SET json_data = jsonb_set(json_data, '{%s}', (json_data->'%s') - ?), data_changed=NOW()::timestamp"
                        + " WHERE id_uuid=?"
                        + " RETURNING *;";

        DELETE_RECORD = "DELETE FROM public.%s WHERE id_uuid=?";
    }

    public int deleteRecord(Connection connection, String tableName, String idUuid) {
        String queryString = String.format(DELETE_RECORD, tableName);
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryString)) {
            preparedStatement.setString(1, idUuid);
            return preparedStatement.executeUpdate();
        } catch (Exception exc) {
            log.error("Error in method deleteRecord()", exc);
            return 0;
        }
    }

    public DatabaseEntry deleteArrayElement(
            Connection connection, String idUuid, String tableName, String element, Integer deletePlace) {
        String queryString = String.format(DELETE_ARRAY_ELEMENT, tableName, element, element);
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryString)) {
            int index = 1;
            preparedStatement.setInt(index++, deletePlace);
            preparedStatement.setString(index, idUuid);
            return extractDbEntryResultSet(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method deleteArrayElement()", exc);
            return null;
        }
    }

    public DatabaseEntry updateData(
            Connection connection, String uuid, String tableName, String data) {
        String insertSql = String.format(UPDATE_BY_UUID, tableName);
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertSql)) {
            String cleanedData = data.replaceAll("\\r\\n", "");
            int index = 1;
            preparedStatement.setObject(index++, cleanedData);
            preparedStatement.setString(index, uuid);
            return extractDbEntryResultSet(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method updateData()", exc);
            return null;
        }
    }

    public DatabaseEntry insertData(
            Connection connection, String uuid, String tableName, String data, Integer tableDefId) {
        String insertSql = String.format(INSERT_DATA, tableName);
        Long startTime = System.currentTimeMillis();
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertSql)) {

            LocalDateTime localDateTime = new LocalDateTime();
            String cleanedData = data.replaceAll("\\r\\n", "");

            int index = 1;
            preparedStatement.setInt(index++, tableDefId);
            preparedStatement.setObject(index++, cleanedData);
            preparedStatement.setString(index++, uuid);
            preparedStatement.setTimestamp(index++, dateUtil.jodaTimeToSQLDate(localDateTime));
            preparedStatement.setNull(index, Types.TIMESTAMP);
            return extractDbEntryResultSet(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method insertData()", exc);
            return null;
        } finally {
            Long endTime = System.currentTimeMillis();
            Long duration = (endTime - startTime);
            log.debug("Duration of insertData(): {} ms", duration);
        }
    }

    public DatabaseEntry selectDataByIdUuid(Connection connection, String tableName, String idUuid) {
        String queryString = String.format(SELECT_BY_IDUUID, tableName);
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryString)) {
            preparedStatement.setString(1, idUuid);
            return extractDbEntryResultSet(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectDataByIdUuid()", exc);
            return null;
        }
    }

    public List<DatabaseEntry> searchData(
            Connection connection,
            String tableName,
            String order,
            Integer limit,
            Integer offset,
            JsonElement searchObject,
            List<IndexInfo> indexes,
            LogicalOperator logicalOperator) {
        try {
            log.trace("searchData(), searchObject: {}", searchObject);
            if (searchObject == null) {
                return select(connection, tableName, limit, offset, order);
            } else {
                SearchTerm term = createTerm(searchObject.getAsJsonObject(), indexes, limit, logicalOperator);
                log.trace("finalTerm: {}", term.getSearchTerm());
                return select(connection, tableName, term.getLimit(), offset, term.getSearchTerm(), order);
            }
        } catch (Exception exc) {
            log.error("Error in method searchData()", exc);
            return new ArrayList<>();
        }
    }

    public SearchTerm createTerm(JsonObject searchTerm, List<IndexInfo> indexes, Integer limit, LogicalOperator logicalOperator)
            throws PostgreJsonException {

        log.debug("Logical operator: {}", logicalOperator);

        String ftsQueryTerm =
                "to_tsvector('simple', json_data->'%s') @@ to_tsquery('simple', '%s')"; // Value
        // <"key":"value">

        String objectTerm = "json_data->'%s' @> '%s'"; // json_data->'first_level_key' @>
        // '[{"second_level_key":"second_level_key_value"}]';

        String objectArrayTerm =
                "json_data->'%s' @> ANY (ARRAY %s::jsonb[])"; // json_data->'first_level_key' @>
        // ANY (ARRAY ['[{"second_level_key":"second_level_key_value"}]',
        //  '[{"second_level_key":"second_level_key_value2"}]']::jsonb[])

        String nestedObjectTerm =
                "json_data->'%s'->'%s' @> '%s'"; // json_data->'first_level_key'->'second_level_key' @>
        // '[{"second_level_key":"second_level_key_value"}]';

        String exactTerm = "json_data->>'%s'='%s'"; // json_data->>'key'='value';

        log.trace("Indexes: {}", indexes);

        ArrayList<String> terms = new ArrayList<>();

        for (String key : searchTerm.keySet()) {
            IndexInfo indexInfo = findIndexTypeForKey(indexes, key);
            if (indexInfo == null) {
                continue;
            }

            log.trace("indexInfo: {}", indexInfo);

            if (indexInfo.getIndexType() == IndexType.exact) {
                terms.add(String.format(exactTerm, key, searchTerm.get(key).getAsString()));
            } else if (indexInfo.getIndexType() == IndexType.object) {
                log.trace("searchTerm: {}", searchTerm.get(key).toString());
                ArrayList<String> objectTerms = extractSearchTerm(searchTerm.get(key));
                if (objectTerms.size() > 1) {
                    terms.add(String.format(objectArrayTerm, key, objectTerms));
                    limit = null;
                    log.debug("Make limit null");
                } else {
                    terms.add(String.format(objectTerm, key, searchTerm.get(key).toString()));
                }
            } else if (indexInfo.getIndexType() == IndexType.nestedobject) {
                JsonObject nestedTerm = null;
                String nestedKey = null;
                try {

                    JsonObject nestedObject = searchTerm.getAsJsonObject(key);
                    if (nestedObject != null && !nestedObject.isJsonNull()) {
                        log.debug("Key: {} has nested object: {}", key, nestedObject.keySet());
                        nestedKey = nestedObject.keySet().iterator().next();
                        log.debug("First key from {} is {}", key, nestedKey);
                        nestedTerm = nestedObject;
                    }
                } catch (java.lang.ClassCastException exc) {
                    log.trace("exc", exc);
                }

                if(nestedTerm != null) {
                    terms.add(String.format(nestedObjectTerm, key, nestedKey, nestedTerm.get(nestedKey).toString()));
                }
            } else if (indexInfo.getIndexType() == IndexType.fts && searchTerm.get(key).isJsonPrimitive()) {
                log.debug("IndexType.fts key: '{}', term: '{}'", key, searchTerm.get(key).getAsString());
                terms.add(String.format(ftsQueryTerm, key, searchTerm.get(key).getAsString()));
            }
        }
        String finalTerm = "";
        log.trace("Terms: {}", terms);
        log.debug("Logical operator property name: {}", logicalOperator.getPropertyName());
        if (terms.size() > 1) {
            String separator = String.format(" %s ", logicalOperator.getPropertyName());
            finalTerm = StringUtils.join(terms, separator);
            log.debug("Final term: {}", finalTerm);
            return new SearchTerm(finalTerm, limit);
        } else if (terms.size() == 1) {
            log.debug("Term size = 1");
            return new SearchTerm(terms.get(0), limit);
        } else {
            throw new PostgreJsonException("Could not generate search term");
        }
    }

    private ArrayList<String> extractSearchTerm(JsonElement objectTerms) {
        try {
            JsonArray array = objectTerms.getAsJsonArray();
            ArrayList<String> composedTermList = new ArrayList<>();
            for (JsonElement element : array) {
                log.trace("single-element: {}", element.toString());
                composedTermList.add(String.format("'[%s]'", element.toString()));
            }
            log.trace("composedTermList: {}", composedTermList);
            return composedTermList;
        } catch (Exception exc) {
            return new ArrayList<>();
        }
    }

    private IndexInfo findIndexTypeForKey(List<IndexInfo> indexes, String key) {
        log.trace("searching for: {}", key);
        for (IndexInfo indexInfo : indexes) {
            String indexName = indexInfo.getIndexName();
            ArrayList<String> nameList = new ArrayList<>(Arrays.asList(indexName.split("_")));
            if (!nameList.contains(key.toLowerCase())) {
                continue;
            }
            log.trace("nameList: {}", nameList);
            nameList.remove(0);
            log.trace("nameList.get(0) : {}", nameList.get(0));
            indexInfo.setIndexType(nameList.get(0));
            nameList.remove(0);
            if (nameList.size() > 1) {
                indexInfo.setKeyPath(StringUtils.join(nameList, "."));
            } else {
                indexInfo.setKeyPath(nameList.get(0));
            }
            return indexInfo;
        }
        return null;
    }

    private ArrayList<DatabaseEntry> select(
            Connection connection,
            String tableName,
            Integer limit,
            Integer offset,
            String term,
            String order) {

        String queryString;
        if (limit != null) {
            queryString = String.format(SELECT_ALL_BY_TERM_WITH_LIMIT, tableName, term, order);
            log.debug("Limit is not null, selecting with limit ({}) and offset ({})", limit, offset);
        } else {
            queryString = String.format(SELECT_ALL_BY_TERM, tableName, term);
            log.debug("Limit is null, selecting without limit and offset");
        }
        log.debug("queryString: {}", queryString);
        Long startTime = System.currentTimeMillis();
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryString)) {
            Long timeOfStatementPrepare = System.currentTimeMillis();
            Long durationOfStatementPrepare = (timeOfStatementPrepare - startTime);
            log.debug("Duration of connection.prepareStatement: {} ms", durationOfStatementPrepare);
            if (limit != null) {
                int index = 1;
                preparedStatement.setInt(index++, limit);
                preparedStatement.setInt(index, offset);
            }
            return executeDatabaseEntryListPrepStatement(preparedStatement, limit);
        } catch (Exception exc) {
            log.error("Error in method select(SELECT_ALL_BY_TERM)", exc);
            return new ArrayList<>();
        } finally {
            Long endTime = System.currentTimeMillis();
            Long duration = (endTime - startTime);
            log.debug("Duration of select(SELECT_ALL_BY_TERM): {} ms", duration);
        }
    }

    private ArrayList<DatabaseEntry> select(
            Connection connection, String tableName, Integer limit, Integer offset, String order) {
        String queryString = String.format(SELECT_ALL, tableName, order);
        log.debug("queryString: {}", queryString);
        Long startTime = System.currentTimeMillis();
        try (PreparedStatement preparedStatement = connection.prepareStatement(queryString)) {
            int index = 1;
            preparedStatement.setInt(index++, limit);
            preparedStatement.setInt(index, offset);
            return executeDatabaseEntryListPrepStatement(preparedStatement, limit);
        } catch (Exception exc) {
            log.error("Error in method select(SELECT_ALL)", exc);
            return new ArrayList<>();
        } finally {
            Long endTime = System.currentTimeMillis();
            Long duration = (endTime - startTime);
            log.debug("Duration of select(SELECT_ALL): {} ms", duration);
        }
    }

    private ArrayList<DatabaseEntry> executeDatabaseEntryListPrepStatement(
            PreparedStatement preparedStatement, Integer fetchSize) throws SQLException {
        Long startTime = System.currentTimeMillis();
        if (fetchSize != null) {
            preparedStatement.setFetchSize(fetchSize);
        }
        try (ResultSet resultSet = preparedStatement.executeQuery()) {

            Long endTime = System.currentTimeMillis();
            Long duration = (endTime - startTime);
            log.debug("Duration of executeDatabaseEntryListPrepStatement (query to DB only): {} ms", duration);

            ArrayList<DatabaseEntry> databaseEntryList = new ArrayList<>();

            while (resultSet.next()) {
                databaseEntryList.add(extractDatabaseEntryResultSet(resultSet));
            }
            return databaseEntryList;
        } catch (Exception exc) {
            log.error("Error in method executeDatabaseEntryListPrepStatement()", exc);
            return new ArrayList<>();
        } finally {
            Long endTime = System.currentTimeMillis();
            Long duration = (endTime - startTime);
            log.debug("Duration of executeDatabaseEntryListPrepStatement (including query to DB and fetch): {} ms", duration);
        }
    }

    private DatabaseEntry extractDatabaseEntryResultSet(ResultSet resultSet) {
        Long startTime = System.currentTimeMillis();
        try {
            DatabaseEntry dbEntry = new DatabaseEntry();
            dbEntry.setEntryDbId(resultSet.getInt(DatabaseEntry.ENTRY_DB_ID_COLUMN));
            dbEntry.setSchemaDbId(resultSet.getInt(DatabaseEntry.SCHEMA_DB_ID_COLUMN));
            dbEntry.setDataCreatedDateTime(
                    dateUtil.slqTimeStampToJodaTime(
                            resultSet.getTimestamp(DatabaseEntry.DATA_CREATED_DATE_TIME_COLUMN)));
            dbEntry.setDataChangedDateTime(
                    dateUtil.slqTimeStampToJodaTime(
                            resultSet.getTimestamp(DatabaseEntry.DATA_CHANGED_DATE_TIME_COLUMN)));
            dbEntry.setEntryIdUuid(resultSet.getString(DatabaseEntry.ENTRY_ID_UUID_COLUMN));
            dbEntry.setJsonData(resultSet.getString(DatabaseEntry.JSON_DATA_COLUMN));
            return dbEntry;
        } catch (Exception exc) {
            log.error("Error in method extractDatabaseEntryResultSet()", exc);
            return null;
        } finally {
            Long endTime = System.currentTimeMillis();
            Long duration = (endTime - startTime);
            log.trace("Duration of extractDatabaseEntryResultSet: {} ms", duration);
        }
    }

    private DatabaseEntry extractDbEntryResultSet(PreparedStatement preparedStatement) {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            if (resultSet.next()) {
                return extractDatabaseEntryResultSet(resultSet);
            }
            log.debug("DatabaseEntry result set was empty returning NULL");
            return null;
        } catch (Exception exc) {
            log.error("Error in method extractDbEntryResultSet()", exc);
            return null;
        }
    }
}
