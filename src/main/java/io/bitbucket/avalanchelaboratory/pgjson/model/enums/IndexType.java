package io.bitbucket.avalanchelaboratory.pgjson.model.enums;

public enum IndexType {
    object("object"),
    nestedobject("nestedobject"),
    exact("exact"),
    fts("fts");

    private final String propertyName;

    private IndexType(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return this.propertyName;
    }
}
