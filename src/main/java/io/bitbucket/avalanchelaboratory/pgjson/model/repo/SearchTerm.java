package io.bitbucket.avalanchelaboratory.pgjson.model.repo;

import io.bitbucket.avalanchelaboratory.pgjson.model.enums.LogicalOperator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchTerm {
    String searchTerm;
    Integer limit;
}
