package io.bitbucket.avalanchelaboratory.pgjson.model.enums;

public enum LogicalOperator {
    or("OR"),
    and("AND");

    private final String propertyName;

    private LogicalOperator(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return this.propertyName;
    }
}
