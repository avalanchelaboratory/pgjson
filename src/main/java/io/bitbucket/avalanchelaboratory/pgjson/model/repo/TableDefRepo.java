package io.bitbucket.avalanchelaboratory.pgjson.model.repo;

import io.bitbucket.avalanchelaboratory.pgjson.model.IndexInfo;
import io.bitbucket.avalanchelaboratory.pgjson.model.TableDef;
import io.bitbucket.avalanchelaboratory.pgjson.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDateTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Slf4j
public class TableDefRepo {

    protected static final String INSERT_TABLEDEF;
    protected static final String SELECT_BY_ATTRIBUTE;
    protected static final String SELECT_TABLE_DEF_BY_UUID;
    protected static final String SELECT_FOR_TEST;
    protected static final String SELECT_ALL_EFFECTIVE_TABLE_DEF;
    protected static final String CREATE_INDEX;
    protected static final String SELECT_INDEX;
    protected static final String SELECT_TABLE_DEF_BY_ID;
    protected static final String SELECT_TABLEDEF_BY_TABLE_NAME;
    protected static final String SELECT_TABLEDEF_BY_TABLE_NAME_AND_SCHEMA_NAME;

    private DateUtil dateUtil = new DateUtil();

    static {
        INSERT_TABLEDEF =
                "INSERT INTO public.tabledef (id_uuid, schema, schema_name, table_name, schema_timestamp, schema_hash) VALUES (?, ?, ?, ?, ?, ?);";

        SELECT_BY_ATTRIBUTE =
                "SELECT * FROM public.%s WHERE json_data @> '%s' ORDER BY id DESC LIMIT 1;";

        SELECT_TABLE_DEF_BY_UUID =
                "SELECT * FROM  public.tabledef WHERE id_uuid=? ORDER BY id DESC LIMIT 1;";

        SELECT_TABLE_DEF_BY_ID = "SELECT * FROM  public.tabledef WHERE id=?;";

        SELECT_TABLEDEF_BY_TABLE_NAME = "SELECT * FROM  public.tabledef WHERE table_name=? ORDER BY id DESC LIMIT 1;";

        SELECT_TABLEDEF_BY_TABLE_NAME_AND_SCHEMA_NAME = "SELECT * FROM  public.tabledef WHERE table_name=? AND schema_name=? ORDER BY id DESC LIMIT 1; ";

        SELECT_FOR_TEST = "SELECT 1;";

        SELECT_ALL_EFFECTIVE_TABLE_DEF =
                "SELECT DISTINCT ON (schema_name) * FROM public.tabledef ORDER BY schema_name, id DESC;";

        CREATE_INDEX = "CREATE INDEX IF NOT EXISTS %s ON %s USING GIN (((json_data -> '%s'::text)));";

        SELECT_INDEX =
                "SELECT indexname, indexdef FROM pg_indexes WHERE tablename = ?  AND indexname LIKE 'pgjson_%';;";
    }

    public Integer createIndexOnJsonObject(
            Connection connection, String indexName, String tableName, String attribute) {
        String insertSql = String.format(CREATE_INDEX, indexName, tableName, attribute);
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertSql)) {
            return preparedStatement.executeUpdate();
        } catch (Exception exc) {
            log.error("Error in method createIndexOnJsonObject()", exc);
            return 0;
        }
    }

    public ArrayList<IndexInfo> selectAllIndexOfTable(Connection connection, String tableName) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INDEX)) {
            int index = 1;
            preparedStatement.setString(index++, tableName);
            return executeIndexListPreparedStatement(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectEffectiveTableDef()", exc);
            return new ArrayList<>();
        }
    }

    private ArrayList<IndexInfo> executeIndexListPreparedStatement(PreparedStatement preparedStatement) {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            ArrayList<IndexInfo> indexInfoList = new ArrayList<>();
            while (resultSet.next()) {
                IndexInfo indexInfo = new IndexInfo();
                indexInfo.setIndexName(resultSet.getString(IndexInfo.COLUMN_INDEX_NAME));
                indexInfo.setIndexDef(resultSet.getString(IndexInfo.COLUMN_INDEX_DEF));
                indexInfoList.add(indexInfo);
            }
            return indexInfoList;
        } catch (Exception exc) {
            log.error("Error in method executeIndexListPreparedStatement()", exc);
            return new ArrayList<>();
        }
    }

    public Integer insertSchema(
            Connection connection,
            String uuid,
            String tableName,
            String schemaName,
            String schemaData,
            String schemaHash) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TABLEDEF)) {
            LocalDateTime localDateTime = new LocalDateTime();
            int index = 1;
            preparedStatement.setString(index++, uuid);
            preparedStatement.setString(index++, schemaData);
            preparedStatement.setString(index++, schemaName);
            preparedStatement.setString(index++, tableName);
            preparedStatement.setTimestamp(index++, dateUtil.jodaTimeToSQLDate(localDateTime));
            preparedStatement.setString(index, schemaHash);
            return preparedStatement.executeUpdate();
        } catch (Exception exc) {
            log.error("Error in method insertSchema()", exc);
            return 0;
        }
    }

    public TableDef selectTableDefByUuid(Connection connection, String tableDefUuid) {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_TABLE_DEF_BY_UUID)) {
            preparedStatement.setString(1, tableDefUuid);
            return executeTableDefPrepStatement(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectTableDefByUuid()", exc);
            return null;
        }
    }

    public TableDef selectTableDefById(Connection connection, Integer tableDefId) {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_TABLE_DEF_BY_ID)) {
            preparedStatement.setInt(1, tableDefId);
            return executeTableDefPrepStatement(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectTableDefById()", exc);
            return null;
        }
    }

    public TableDef selectTableDefByTableName(Connection connection, String tableName) {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_TABLEDEF_BY_TABLE_NAME)) {
            preparedStatement.setString(1, tableName);
            return executeTableDefPrepStatement(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectTableDefByTableName()", exc);
            return null;
        }
    }

    public TableDef selectTableDefByTableNameAndName(Connection connection, String tableName, String schemaName) {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_TABLEDEF_BY_TABLE_NAME_AND_SCHEMA_NAME)) {
            preparedStatement.setString(1, tableName);
            preparedStatement.setString(2, schemaName);
            return executeTableDefPrepStatement(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectTableDefByTableNameAndName()", exc);
            return null;
        }
    }


    public ArrayList<TableDef> selectAllEffectiveTableDef(Connection connection) {
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_ALL_EFFECTIVE_TABLE_DEF)) {
            return executeTableDefListPrepStatement(preparedStatement);
        } catch (Exception exc) {
            log.error("Error in method selectEffectiveTableDef()", exc);
            return new ArrayList<>();
        }
    }

    private TableDef extractTableDefResultSet(ResultSet resultSet) {
        try {
            TableDef tableDef = new TableDef();
            tableDef.setTableDefId(resultSet.getInt(TableDef.TABLE_DEF_ID_COLUMN));
            tableDef.setIdUuid(resultSet.getString(TableDef.ID_UUID_COLUMN));
            tableDef.setSchemaData(resultSet.getString(TableDef.SCHEMA_COLUMN));
            tableDef.setSchemaName(resultSet.getString(TableDef.SCHEMA_NAME_COLUMN));
            tableDef.setTableName(resultSet.getString(TableDef.TABLE_NAME_COLUMN));
            tableDef.setSchemaDate(
                    dateUtil.slqTimeStampToJodaTime(resultSet.getTimestamp(TableDef.SCHEMA_DATE_COLUMN)));
            tableDef.setSchemaHash(resultSet.getString(TableDef.SCHEMA_HASH_COLUMN));
            return tableDef;
        } catch (Exception exc) {
            log.error("Error in method extractTableDefResultSet()", exc);
            return null;
        }
    }

    private ArrayList<TableDef> executeTableDefListPrepStatement(PreparedStatement preparedStatement) {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            ArrayList<TableDef> tableDefList = new ArrayList<>();
            while (resultSet.next()) {
                tableDefList.add(extractTableDefResultSet(resultSet));
            }
            return tableDefList;
        } catch (Exception exc) {
            log.error("Error in method executeTableDefPrepStatement()", exc);
            return new ArrayList<>();
        }
    }

    private TableDef executeTableDefPrepStatement(PreparedStatement preparedStatement) {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            log.debug("TableDef resultSet size: {}", resultSet.getFetchSize());
            if (resultSet.next()) {
                return (extractTableDefResultSet(resultSet));
            } else {
                log.debug("TableDef result set was empty returning NULL");
                return null;
            }
        } catch (Exception exc) {
            log.error("Error in method executeTableDefPrepStatement()", exc);
            return null;
        }
    }

    public boolean selectTestFromTableDef(Connection connection) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FOR_TEST)) {
            return preparedStatement.execute();
        } catch (Exception exc) {
            log.error("Error in method selectTestFromTableDef()", exc);
            return false;
        }
    }
}
