package io.bitbucket.avalanchelaboratory.pgjson.model.operations;

import io.bitbucket.avalanchelaboratory.pgjson.model.validation.ValidationResult;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Result {
    private String iduuid;
    private String finalJsonData;
    private String tableName;
    private ValidationResult validationResult;
    private Boolean resultStatus;
    private String resultMessage;
}
