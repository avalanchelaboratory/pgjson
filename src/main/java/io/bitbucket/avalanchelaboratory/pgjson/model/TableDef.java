package io.bitbucket.avalanchelaboratory.pgjson.model;

import lombok.Data;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;

@Data
public class TableDef {

    public static final String TABLE_DEF_ID_COLUMN = "id";
    public static final String ID_UUID_COLUMN = "id_uuid";
    public static final String SCHEMA_COLUMN = "schema";
    public static final String SCHEMA_NAME_COLUMN = "schema_name";
    public static final String TABLE_NAME_COLUMN = "table_name";
    public static final String SCHEMA_DATE_COLUMN = "schema_timestamp";
    public static final String SCHEMA_HASH_COLUMN = "schema_hash";
    public static final String TABLE_NAME = "tabledef";

    private Integer tableDefId;
    private String idUuid;
    private String schemaData;
    private String schemaName;
    private String tableName;
    private LocalDateTime schemaDate;
    private String schemaHash;
    private ArrayList<IndexInfo> indexInfo;
}
