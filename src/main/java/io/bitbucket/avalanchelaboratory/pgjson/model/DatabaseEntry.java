package io.bitbucket.avalanchelaboratory.pgjson.model;

import lombok.Data;
import org.joda.time.LocalDateTime;

import java.io.Serializable;

@Data
public class DatabaseEntry implements Serializable {

    public static final String ENTRY_DB_ID_COLUMN = "id";
    public static final String SCHEMA_DB_ID_COLUMN = "tabledef_id";
    public static final String JSON_DATA_COLUMN = "json_data";
    public static final String ENTRY_ID_UUID_COLUMN = "id_uuid";
    public static final String DATA_CREATED_DATE_TIME_COLUMN = "data_created";
    public static final String DATA_CHANGED_DATE_TIME_COLUMN = "data_changed";

    Integer entryDbId;
    Integer schemaDbId;
    String jsonData;
    String entryIdUuid;
    LocalDateTime dataCreatedDateTime;
    LocalDateTime dataChangedDateTime;
}
