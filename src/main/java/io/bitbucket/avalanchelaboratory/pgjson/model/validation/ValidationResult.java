package io.bitbucket.avalanchelaboratory.pgjson.model.validation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationResult {
    private Boolean validationStatus;
    private String validationMessage;
}
