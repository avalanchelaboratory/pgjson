package io.bitbucket.avalanchelaboratory.pgjson.model;

import io.bitbucket.avalanchelaboratory.pgjson.model.enums.IndexType;
import lombok.Data;

@Data
public class IndexInfo {

    public static final String COLUMN_INDEX_NAME = "indexname";
    public static final String COLUMN_INDEX_DEF = "indexdef";

    private String indexName;
    private String indexDef;
    private IndexType indexType;
    private String keyPath;

    public void setIndexType(String indexTypeString) {
        this.indexType = IndexType.valueOf(indexTypeString);
    }
}
