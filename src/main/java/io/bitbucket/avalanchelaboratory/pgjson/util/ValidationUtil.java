package io.bitbucket.avalanchelaboratory.pgjson.util;

import io.bitbucket.avalanchelaboratory.pgjson.model.validation.ValidationResult;
import lombok.extern.slf4j.Slf4j;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Slf4j
public class ValidationUtil {

    InputStream getStream(String data) {
        return new ByteArrayInputStream(data.getBytes());
    }

    public ValidationResult validateData(String schemaData, String data) {
        try (InputStream schemaStream = getStream(schemaData)) {
            log.debug("Validating data");
            log.trace("Data: {}", data);
            JSONObject rawSchema = new JSONObject(new JSONTokener(schemaStream));
            Schema schema = SchemaLoader.load(rawSchema);
            schema.validate(new JSONObject(data));
            log.debug("Validation succeeded");
            return new ValidationResult(true, "Data is valid");
        } catch (org.everit.json.schema.ValidationException exc) {
            log.debug("Message: {}", exc.getMessage());
            log.debug("{}", exc.getCausingExceptions());
            String errorMessage = String.join(" | ", exc.getAllMessages());
            return new ValidationResult(false, errorMessage);
        } catch (Exception exc) {
            log.warn("Error in method validateData", exc);
            return new ValidationResult();
        }
    }
}
