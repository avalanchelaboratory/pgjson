package io.bitbucket.avalanchelaboratory.pgjson.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Slf4j
public class IndexUtil {

    private Gson gson = new Gson();

    public Map<String, String> getIndexes(String schema) {

        JsonObject tableDefDataObject = gson.fromJson(schema, JsonObject.class);
        JsonObject definitionsObject = tableDefDataObject.getAsJsonObject("definitions");
        JsonObject propertiesObject = tableDefDataObject.getAsJsonObject("properties");

        log.debug("Deffinitions: {}", definitionsObject);

        Map<String, String> indexMap = new HashMap<>();
        JsonObject indexObject = new JsonObject();
        detectObject(null, propertiesObject, indexObject, indexMap, null);
        log.debug("");
        log.debug("indexMap: {}", indexMap);
        log.debug("indexObject: {}", indexObject);
        return new HashMap<>();
    }

    private void detectObject(
            String keyer,
            JsonObject propertiesObject,
            JsonObject indexObject,
            Map<String, String> indexMap,
            String parentKey) {
        log.debug("keyer: {}, keyset: {}", keyer, propertiesObject.keySet());
        for (String key : propertiesObject.keySet()) {
            try {
                JsonObject object = propertiesObject.getAsJsonObject(key);
                Boolean hasRef = object.has("$ref");
                Boolean hasIndex = object.has("index");
                Boolean hasProps = object.has("properties");
                Boolean hasType = object.has("type");

                if (!hasRef && !hasIndex && !hasProps) {
                    continue;
                }

                if (hasIndex) {
                    String keyPath = "";
                    if (parentKey == null) {
                        keyPath = key;
                    } else {
                        keyPath = String.format("%s->%s", parentKey, key);
                    }
                    indexMap.put(keyPath, object.get("index").getAsString());

                    JsonObject obj = new JsonObject();
                    obj.addProperty("indexType", object.get("index").getAsString());
                }

                if (hasRef) {
                    // Go inside deffinitions
                }

                if (hasProps) {
                    log.debug("enter again to detectObject: {}", key);

                    detectObject(key, object.get("properties").getAsJsonObject(), indexObject, indexMap, key);
                    log.debug("leaving detectObject: {}", key);
                }

                log.debug("{} -> ref: {}, index: {}, props: {}", key, hasRef, hasIndex, hasProps);
            } catch (ClassCastException exc) {
                // log.warn("Not object: {}", key);
            }
        }
    }

    private void getIndexedItems(
            String keyPath,
            Map<String, String> indexCollectionObject,
            JsonObject indexCollectionObject2,
            Set<Map.Entry<String, JsonElement>> entrySet) {
        // Iterate through entry set
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            JsonElement entryElement = entry.getValue();
            log.debug("Element: {}", entryElement);
            // If child is an object
            if (entryElement.isJsonObject()) {
                // Save its key
                String key = entry.getKey();
                JsonObject entryObject = entryElement.getAsJsonObject();
                JsonElement uiLabelElement = entryObject.get("index");
                // If child contains a "index" field
                if (uiLabelElement != null) {
                    String indexType = uiLabelElement.getAsString();
                    indexCollectionObject.put(key, indexType);
                }
                // Repeat
                getIndexedItems(
                        keyPath, indexCollectionObject, indexCollectionObject2, entryObject.entrySet());
            }
        }
    }
}
