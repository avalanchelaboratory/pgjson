package io.bitbucket.avalanchelaboratory.pgjson.util;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.sql.Date;
import java.sql.Timestamp;

public class DateUtil {

    public Timestamp jodaTimeToSQLDate(LocalDateTime localDateTime) {
        return new Timestamp(localDateTime.toDateTime().getMillis());
    }

    public DateTime sqlDateToJodaTime(Date date) {
        return new DateTime(date);
    }

    public LocalDateTime slqTimeStampToJodaTime(Timestamp timeStamp) {
        return new LocalDateTime(timeStamp);
    }
}
