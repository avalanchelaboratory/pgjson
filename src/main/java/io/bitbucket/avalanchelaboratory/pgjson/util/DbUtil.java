package io.bitbucket.avalanchelaboratory.pgjson.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.util.Properties;

@Slf4j
public class DbUtil {

    private HikariDataSource dataSource;
    private Properties props;

    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (Exception exc) {
            log.error("Error in method getConnection, {}", exc.getMessage());
            return null;
        }
    }

    public void closeConnection(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (Exception exc) {
            log.error("Error in method closeConnection, {}", exc.getMessage());
        }
    }

    private void connect() {
        log.debug("Establishing datasource... using {}", props);
        HikariConfig config = new HikariConfig(props);
        props.setProperty("dataSource.prepareThreshold", "1"); // Will create client side prepared statements
        dataSource = new HikariDataSource(config);
        log.debug("Data source status is running?: {}", dataSource.isRunning());
        log.debug("Datasource created.");
    }

    public void stop() {
        try {
            log.debug("Disconnecting from data source...");
            if (dataSource != null) {
                dataSource.close();
                log.debug("Data source status is closed?: {}", dataSource.isClosed());
                if (dataSource.isClosed() == false) {
                    log.warn("Data source status is not closed this may cause database connections to idle in database server.");
                }
            }
        } catch (Exception exc) {
            log.error("Error in method {}, {}", exc.getStackTrace()[0].getClassName(), exc);
        }
    }

    public DbUtil(Properties props) {
        this.props = props;
        log.debug("Initializing database connection");
        connect();
        log.debug("Connection initialized");
    }
}
