package io.bitbucket.avalanchelaboratory.pgjson;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import io.bitbucket.avalanchelaboratory.pgjson.exceptions.RequestException;
import io.bitbucket.avalanchelaboratory.pgjson.model.DatabaseEntry;
import io.bitbucket.avalanchelaboratory.pgjson.model.IndexInfo;
import io.bitbucket.avalanchelaboratory.pgjson.model.TableDef;
import io.bitbucket.avalanchelaboratory.pgjson.model.enums.LogicalOperator;
import io.bitbucket.avalanchelaboratory.pgjson.model.operations.Result;
import io.bitbucket.avalanchelaboratory.pgjson.model.repo.DatabaseEntryRepo;
import io.bitbucket.avalanchelaboratory.pgjson.model.repo.TableDefRepo;
import io.bitbucket.avalanchelaboratory.pgjson.model.validation.ValidationResult;
import io.bitbucket.avalanchelaboratory.pgjson.util.DbUtil;
import io.bitbucket.avalanchelaboratory.pgjson.util.JsonUtil;
import io.bitbucket.avalanchelaboratory.pgjson.util.ValidationUtil;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.util.*;

@Slf4j
public class PostgreSqlJsonClient {

    private static final String EXCEPTION_HAPPENED_MESSAGE = "Exception happened";

    private List<TableDef> effectiveTableDef;

    private Gson gson = null;

    private DbUtil dbUtil = null;
    private ValidationUtil validationUtil = null;
    private JsonUtil jsonUtil = null;

    private DatabaseEntryRepo databaseEntryRepo = new DatabaseEntryRepo();
    private TableDefRepo tableDefRepo = new TableDefRepo();

    public PostgreSqlJsonClient(Properties props) {
        try {
            gson = new Gson();
            dbUtil = new DbUtil(props);
            jsonUtil = new JsonUtil();
            validationUtil = new ValidationUtil();
            effectiveTableDef = getAllEffectiveTableDef();
        } catch (Exception exc) {
            log.error("Initialization of PostgreSqlJsonClient failed", exc);
        }
    }

    public Connection getConnection() {
        return dbUtil.getConnection();
    }

    private List<TableDef> getAllEffectiveTableDef() {
        Connection connection = dbUtil.getConnection();
        try {
            log.debug("Loading effective table definitions into memory");
            List<TableDef> returnedTableDefs = new ArrayList<>();
            List<TableDef> tableDefs = tableDefRepo.selectAllEffectiveTableDef(connection);
            for (TableDef item : tableDefs) {
                ArrayList<IndexInfo> indexes =
                        tableDefRepo.selectAllIndexOfTable(connection, item.getTableName());
                item.setIndexInfo(indexes);
                returnedTableDefs.add(item);
            }
            return returnedTableDefs;
        } catch (Exception exc) {
            log.error("Exception in method getEffectiveTableDef", exc);
            return new ArrayList<>();
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public TableDef getTableDefByIdUuidFromMemory(String idUuid) {
        try {
            TableDef tableDefFound =
                    effectiveTableDef.stream()
                            .filter(tableDef -> idUuid.equals(tableDef.getIdUuid()))
                            .findFirst()
                            .orElse(null);
            if (tableDefFound == null) {
                log.debug(
                        "Did not found TableDef with idUuid from memory, trying to query from database",
                        idUuid);
                tableDefFound = getTableDefFromDatabase(null, idUuid, null, null);
            }
            return tableDefFound;
        } catch (Exception exc) {
            log.error("Exception in method getTableDefByIdUuidFromMemory", exc);
            return new TableDef();
        }
    }

    public TableDef getTableDefByTableNameAndNameFromMemory(String tableName, String schemaName) {
        try {
            TableDef tableDefFound = null;
            if (schemaName != null) {
                tableDefFound =
                        effectiveTableDef.stream()
                                .filter(tableDef -> tableName.equals(tableDef.getTableName()))
                                .filter(tableDef -> schemaName.equals(tableDef.getSchemaName()))
                                .findFirst()
                                .orElse(null);
            } else {
                tableDefFound =
                        effectiveTableDef.stream()
                                .filter(tableDef -> tableName.equals(tableDef.getTableName()))
                                .findFirst()
                                .orElse(null);
            }
            if (tableDefFound == null) {
                log.debug(
                        "Did not found TableDef with table_name from memory, trying to query from database",
                        tableName);
                tableDefFound = getTableDefFromDatabase(null, null, tableName, schemaName);
            }
            log.debug("Found TableDef: {}", tableDefFound);
            return tableDefFound;
        } catch (Exception exc) {
            log.error("Exception in method getTableDefByTableNameFromMemory", exc);
            return new TableDef();
        }
    }

    public TableDef getTableDefByIdFromMemory(Integer tableDefId) {
        try {
            TableDef tableDefFound =
                    effectiveTableDef.stream()
                            .filter(tableDef -> tableDefId.equals(tableDef.getTableDefId()))
                            .findFirst()
                            .orElse(null);

            log.trace("tableDefFound from memory: {}", tableDefFound);
            if (tableDefFound == null) {
                log.debug(
                        "Did not found TableDef with id {} from memory, trying to query from database",
                        tableDefId);
                tableDefFound = getTableDefFromDatabase(tableDefId, null, null, null);
            }
            return tableDefFound;
        } catch (Exception exc) {
            log.error("Exception in method getTableDefByIdFromMemory", exc);
            return new TableDef();
        }
    }

    private TableDef getTableDefFromDatabase(
            Integer tableDefId, String tableDefIdUuid, String tableName, String schemaName) {
        Connection connection = dbUtil.getConnection();
        try {
            TableDef tableDef = null;
            if (tableDefId != null) {
                tableDef = tableDefRepo.selectTableDefById(connection, tableDefId);
            }
            if (tableDefIdUuid != null) {
                tableDef = tableDefRepo.selectTableDefByUuid(connection, tableDefIdUuid);
            }
            if (tableName != null && schemaName != null) {
                tableDef = tableDefRepo.selectTableDefByTableNameAndName(connection, tableName, schemaName);
            }
            if (tableName != null && schemaName == null) {
                tableDef = tableDefRepo.selectTableDefByTableName(connection, tableName);
            }
            if (tableDef == null) {
                log.debug("Did not find table def for: {}, {}, {}", tableDefId, tableDefIdUuid, tableName);
                return new TableDef();
            }
            ArrayList<IndexInfo> indexes = tableDefRepo.selectAllIndexOfTable(connection, tableDef.getTableName());
            tableDef.setIndexInfo(indexes);
            return tableDef;
        } catch (Exception exc) {
            log.error("Exception in method getTableDefFromDatabase", exc);
            return new TableDef();
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public String insertSchema(String tableName, String schemaName, String schemaData) {
        Connection connection = dbUtil.getConnection();
        try {
            UUID uuid = UUID.randomUUID();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            String schemaHash = bytesToHex(digest.digest(schemaData.getBytes(StandardCharsets.UTF_8)));

            Integer result =
                    tableDefRepo.insertSchema(
                            connection, uuid.toString(), tableName, schemaName, schemaData, schemaHash);
            if (result > 0) {
                return uuid.toString();
            } else {
                return null;
            }
        } catch (Exception exc) {
            log.error("Exception in method insertSchema", exc);
            return null;
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public Boolean deleteData(String tableName, String idUuid) {
        Connection connection = dbUtil.getConnection();
        try {
            Integer result = databaseEntryRepo.deleteRecord(connection, tableName, idUuid);

            if (result > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception exc) {
            log.error("Exception in method deleteData", exc);
            return false;
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public Result insertData(String tableName, String jsonData) {
        return insertDataImpl(tableName, null, jsonData);
    }

    public Result insertData(String tableName, String schemaName, String jsonData) {
        return insertDataImpl(tableName, schemaName, jsonData);
    }

    public Result insertDataImpl(String tableName, String schemaName, String jsonData) {
        Connection connection = dbUtil.getConnection();
        try {
            ValidationResult validationResult = new ValidationResult();
            Boolean resultStatus = false;
            String resultMessage = "";
            UUID entryIdUuid = UUID.randomUUID();
            String entryIdUuidString = entryIdUuid.toString();

            TableDef tableDef = getTableDefByTableNameAndNameFromMemory(tableName, schemaName);
            if (tableDef != null) {
                validationResult = validationUtil.validateData(tableDef.getSchemaData(), jsonData);
                if (Boolean.TRUE.equals(validationResult.getValidationStatus())) {
                    DatabaseEntry insertedEntryReturned =
                            databaseEntryRepo.insertData(
                                    connection, entryIdUuidString, tableName, jsonData, tableDef.getTableDefId());
                    if (insertedEntryReturned != null) {
                        resultStatus = true;
                        entryIdUuidString = insertedEntryReturned.getEntryIdUuid();
                        jsonData = insertedEntryReturned.getJsonData();
                        resultMessage = "Database entry inserted in database";
                    } else {
                        resultStatus = false;
                        entryIdUuidString = null;
                        jsonData = null;
                        resultMessage = "Database entry did not inserted in database due to SQL error";
                    }
                } else {
                    resultStatus = false;
                    entryIdUuidString = null;
                    jsonData = null;
                    resultMessage = "Data validation failed";
                }
            } else {
                resultMessage = "Table def not found for table";
                log.warn("Table def not found for table {}", tableName);
            }
            return new Result(
                    entryIdUuidString, jsonData, tableName, validationResult, resultStatus, resultMessage);
        } catch (Exception exc) {
            log.error("Exception in method insertData", exc);
            return null;
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public List<DatabaseEntry> selectData(String tableName, String searchJson) throws RequestException {
        return selectDataImpl(tableName, null, searchJson);
    }

    public List<DatabaseEntry> selectData(String tableName, String schemaName, String searchJson) throws RequestException {
        return selectDataImpl(tableName, schemaName, searchJson);
    }

    private List<DatabaseEntry> selectDataImpl(String tableName, String schemaName, String searchJson)
            throws RequestException {
        TableDef tableDef = getTableDefByTableNameAndNameFromMemory(tableName, schemaName);

        JsonObject jsonObject = gson.fromJson(searchJson, JsonObject.class);
        if (jsonObject != null && !jsonObject.isJsonNull()) {
            String order = "asc";
            JsonElement orderTypeElement = jsonObject.get("orderType");
            if (orderTypeElement != null && !orderTypeElement.isJsonNull()) {
                String orderType = orderTypeElement.getAsString();
                if (orderType.equalsIgnoreCase("asc") || orderType.equalsIgnoreCase("desc")) {
                    order = orderType;
                }
            }

            LogicalOperator logicalOperatorType = LogicalOperator.and;
            JsonElement logicalOperatorElement =  jsonObject.get("logicalOperator");
            if (logicalOperatorElement != null && !logicalOperatorElement.isJsonNull()) {
                String logicalOperator = logicalOperatorElement.getAsString();
                if (logicalOperator.equalsIgnoreCase("AND")) {
                    logicalOperatorType = logicalOperatorType.and;
                } else if (logicalOperator.equalsIgnoreCase("OR")) {
                    logicalOperatorType = logicalOperatorType.or;
                }
            }

            Integer limit = jsonObject.get("limit").getAsInt();
            if (limit == null || limit < 1) {
                throw new RequestException(
                        "Query limit is missing or value invalid, should be at least 1.");
            }

            Integer offset = jsonObject.get("offset").getAsInt();
            if (offset == null || offset < 0) {
                throw new RequestException(
                        "Query offset is missing or value is invalid, should be at least 0");
            }

            JsonElement element = jsonObject.get("searchTerm");
            if (!(element instanceof JsonNull)) {
                JsonObject propertyToBeCopied = (JsonObject) element;
                if (propertyToBeCopied == null || propertyToBeCopied.isJsonNull()) {
                    throw new RequestException("Faulty request");
                }
                Set<String> keySet = propertyToBeCopied.keySet();
                if (keySet.isEmpty()) {
                    return searchData(tableName,
                            order,
                            limit,
                            offset,
                            null,
                            tableDef.getIndexInfo(),
                            logicalOperatorType);
                }
                return searchData(tableName,
                        order,
                        limit,
                        offset,
                        propertyToBeCopied,
                        tableDef.getIndexInfo(),
                        logicalOperatorType);
            } else {
                return searchData(tableName, order, limit, offset, null, tableDef.getIndexInfo(), logicalOperatorType);
            }
        } else {
            throw new RequestException("Request was empty");
        }
    }

    private List<DatabaseEntry> searchData(String tableName,
                                           String order,
                                           Integer limit,
                                           Integer offset,
                                           JsonElement searchObject,
                                           ArrayList<IndexInfo> indexes,
                                           LogicalOperator logicalOperator) {
        Connection connection = dbUtil.getConnection();
        try {
            return databaseEntryRepo.searchData(
                    connection, tableName, order, limit, offset, searchObject, indexes, logicalOperator);
        } catch (Exception exc) {
            log.error("Exception in method searchData", exc);
            return new ArrayList<>();
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public Result updateDataWithMerge(String tableName, String data, String entryIdUuid) {
        Connection connection = dbUtil.getConnection();
        try {
            DatabaseEntry databaseEntry = selectDataByIdUuid(entryIdUuid, tableName);
            if (databaseEntry == null) {
                log.warn("Entry not found with idUuid {} in database table {}", entryIdUuid, tableName);
                return new Result(entryIdUuid, null, tableName, null, false, "Databse entry not found");
            }
            log.trace("oldJsonData: {}", databaseEntry.getJsonData());
            String jsonData = jsonUtil.mergeObjects(databaseEntry.getJsonData(), data);
            databaseEntry.setJsonData(jsonData);
            log.trace("newJsonData: {}", jsonData);
            return doValidationAndUpdate(connection, tableName, databaseEntry);
        } catch (Exception exc) {
            log.error("Exception in method updateData", exc);
            return new Result(entryIdUuid, null, tableName, null, false, EXCEPTION_HAPPENED_MESSAGE);
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public Result updateData(String tableName, String jsonData, String entryIdUuid) {
        Connection connection = dbUtil.getConnection();
        try {
            DatabaseEntry databaseEntry = selectDataByIdUuid(entryIdUuid, tableName);
            if (databaseEntry == null) {
                log.warn("Entry not found with idUuid {} in database table {}", entryIdUuid, tableName);
                return new Result(entryIdUuid, null, tableName, null, false, "Databse entry not found");
            }
            databaseEntry.setJsonData(jsonData);
            log.trace("jsonDataToBeUpdate: {}", databaseEntry.getJsonData());
            return doValidationAndUpdate(connection, tableName, databaseEntry);

        } catch (Exception exc) {
            log.error("Exception in method updateData", exc);
            return new Result(entryIdUuid, null, tableName, null, false, EXCEPTION_HAPPENED_MESSAGE);
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    private Result doValidationAndUpdate(Connection connection, String tableName, DatabaseEntry databaseEntry) {
        ValidationResult validationResult = new ValidationResult();
        Boolean resultStatus = false;
        String resultMessage = "";

        if (databaseEntry.getSchemaDbId() > 0) {
            TableDef tableDef = getTableDefByIdFromMemory(databaseEntry.getSchemaDbId());
            if (tableDef != null) {
                validationResult = validationUtil.validateData(tableDef.getSchemaData(), databaseEntry.getJsonData());
                if (Boolean.TRUE.equals(validationResult.getValidationStatus())) {
                    DatabaseEntry changedEntryReturned = databaseEntryRepo.updateData(connection,
                            databaseEntry.getEntryIdUuid(), tableName, databaseEntry.getJsonData());
                    if (changedEntryReturned != null) {
                        resultStatus = true;
                        resultMessage = "Database entry updated in database";
                    } else {
                        resultStatus = false;
                        resultMessage = "Database entry did not updated in database";
                    }
                } else {
                    resultStatus = false;
                }
            }
        }
        return new Result(databaseEntry.getEntryIdUuid(), databaseEntry.getJsonData(),
                tableName, validationResult, resultStatus, resultMessage);
    }

    public Result deleteArrayElement(String tableName, String data, String entryIdUuid) {
        Connection connection = dbUtil.getConnection();
        try {
            JsonObject dataObject = gson.fromJson(data, JsonObject.class);
            String key = null;
            Integer index = null;
            if (dataObject != null && !dataObject.isJsonNull()) {
                JsonElement keyObject = dataObject.get("key");
                if (keyObject != null && !keyObject.isJsonNull()) {
                    key = keyObject.getAsString();
                }

                JsonElement indexObject = dataObject.get("index");
                if (keyObject != null && !indexObject.isJsonNull()) {
                    index = indexObject.getAsInt();
                }
            }

            log.debug("Deleteing array element {}, index {}", key, index);

            if (key != null && index != null) {
                DatabaseEntry changedEntry =
                        databaseEntryRepo.deleteArrayElement(
                                connection, entryIdUuid, tableName, key, index);
                return new Result(
                        changedEntry.getEntryIdUuid(),
                        changedEntry.getJsonData(),
                        tableName,
                        null,
                        true,
                        "Delete done");
            } else {
                return new Result(
                        entryIdUuid, null, tableName, null, false, "Either key or index is missing");
            }
        } catch (Exception exc) {
            log.error("Exception in method deleteArrayElement", exc);
            return new Result(entryIdUuid, null, tableName, null, false, EXCEPTION_HAPPENED_MESSAGE);
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public DatabaseEntry selectDataByIdUuid(String idUuid, String tableName) {
        Connection connection = dbUtil.getConnection();
        try {
            return databaseEntryRepo.selectDataByIdUuid(connection, tableName, idUuid);
        } catch (Exception exc) {
            log.error("Exception in method selectDataByIdUuid", exc);
            return null;
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public TableDef getTableDefByIdUuid(String idUuid) {
        Connection connection = dbUtil.getConnection();
        try {
            return tableDefRepo.selectTableDefByUuid(connection, idUuid);
        } catch (Exception exc) {
            log.error("Exception in method getTableDef", exc);
            return new TableDef();
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    public boolean isDatabaseRunning() {
        Connection connection = dbUtil.getConnection();
        try {
            return tableDefRepo.selectTestFromTableDef(connection);
        } catch (Exception exc) {
            log.error("Exception in method isDatabaseRunning", exc);
            return false;
        } finally {
            dbUtil.closeConnection(connection);
        }
    }

    private void iterateAndSaveUiLabels(JsonObject uiLabelCollectionObject, Set<Map.Entry<String, JsonElement>> entrySet) {
        // Iterate through entry set
        for (Map.Entry<String, JsonElement> entry : entrySet) {
            JsonElement entryElement = entry.getValue();
            // If child is an object
            if (entryElement.isJsonObject()) {
                // Save its key
                String key = entry.getKey();
                JsonObject entryObject = entryElement.getAsJsonObject();
                JsonElement uiLabelElement = entryObject.get("uiLabel");
                // If child contains a "uiLabel" field
                if (uiLabelElement != null) {
                    JsonObject uiLabelObject = uiLabelElement.getAsJsonObject();
                    JsonObject uiLabelDataObject = new JsonObject();
                    // Store its data to an object
                    for (Map.Entry<String, JsonElement> uiLabelEntry : uiLabelObject.entrySet())
                        uiLabelDataObject.add(uiLabelEntry.getKey(), uiLabelEntry.getValue());
                    // Store that object
                    uiLabelCollectionObject.add(key, uiLabelDataObject);
                }
                // Repeat
                iterateAndSaveUiLabels(uiLabelCollectionObject, entryObject.entrySet());
            }
        }
    }

    public String getUiLabels(String tableName) {
        return getUiLabelsImpl(tableName, null);
    }

    public String getUiLabels(String tableName, String schemaName) {
        return getUiLabelsImpl(tableName, schemaName);
    }

    private String getUiLabelsImpl(String tableName, String schemaName) {
        try {
            TableDef tableDef = getTableDefByTableNameAndNameFromMemory(tableName, schemaName);
            if (tableDef != null) {
                log.debug("Found tableDef with idUuid {} ", tableDef.getIdUuid());
                // Object to store labels
                JsonObject uiLabelCollectionObject = new JsonObject();
                // Root schema object
                JsonObject schemaJsonObject = gson.fromJson(tableDef.getSchemaData(), JsonObject.class);

                iterateAndSaveUiLabels(uiLabelCollectionObject, schemaJsonObject.entrySet());

                return uiLabelCollectionObject.toString();
            }
            log.warn("Table def not found for table {}", tableName);
            return null;
        } catch (Exception exc) {
            log.error("Exception in method getUiLabels", exc);
            return null;
        }
    }

    public String getUiLabelsFromSchemaFile(String schema) {
        JsonObject uiLabelCollectionObject = new JsonObject();
        JsonObject schemaJsonObject = gson.fromJson(schema, JsonObject.class);
        iterateAndSaveUiLabels(uiLabelCollectionObject, schemaJsonObject.entrySet());
        log.debug("LABELS: {} ", uiLabelCollectionObject.entrySet().size());
        return uiLabelCollectionObject.toString();
    }

    public void close() {
        try {
            log.debug("Closing postgresql-json database data source.");
            databaseEntryRepo = null;
            if (dbUtil != null) {
                dbUtil.stop();
            }
            dbUtil = null;
            gson = null;
        } catch (Exception exc) {
            log.error("Exception in method close", exc);
        }
    }
}
